## 一、基础

### SPI

SPI ，全称为 Service Provider Interface，是一种服务发现机制。它通过在ClassPath路径下的META-INF/services文件夹查找文件，自动加载文件里所定义的类。

### 虚方法和非虚方法

- 如果方法再编译期就确定了具体的调用版本，这个版本在运行时时不变的。这样的方法称为**非虚方法**。包括：静态方法、私有方法、final方法、实例构造器、父方法。其他的则称为虚方法。

- 虚拟机中提供了以下几条方法调用指令：

  invokestatic：调用静态方法

  invokespecial：调用<init>方法、私有及父方法

  invokevirtual：调用所有虚方法

  invokeinterface：调用接口方法

  invokedynamic：动态调用指令，动态解析出需要调用的方法，然后执行（JDK1.8后才大量使用，主要时lambda表达式）

- 如果在编译时期解析，那么指令指向的方法就是**静态绑定**，也就是private，final，static和构造方法，也就是上面的invokestatic和invokespecial指令，这些在编译器已经确定具体指向的方法。而接口和虚方法调用无法找到真正需要调用的方法，因为它可能是定义在子类中的方法，所以这种在运行时期才能明确类型的方法我们成为**动态绑定**。

### String.intern()



### 值传递和引用传递



## 二、JVM

### 深堆和浅堆



### 深拷贝和浅拷贝



### 强软弱虚引用



## 三、JUC

### 偏向锁、轻量级锁和重量级锁

B栈学习教程P79至P87：https://www.bilibili.com/video/BV16J411h7Rd?p=79

### AQS

全称是 AbstractQueuedSynchronizer，是阻塞式锁和相关的同步器工具的框架

特点：

- 用 state 属性来表示资源的状态（分独占模式和共享模式），子类需要定义如何维护这个状态，控制如何获取锁和释放锁
  - getState - 获取 state 状态
  - setState - 设置 state 状态
  - compareAndSetState - cas 机制设置 state 状态
  - 独占模式是只有一个线程能够访问资源，而共享模式可以允许多个线程访问资源

- 提供了基于 FIFO 的等待队列，类似于 Monitor 的 EntryList

- 条件变量来实现等待、唤醒机制，支持多个条件变量，类似于 Monitor 的 

