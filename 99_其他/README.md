#### 常用命令

------

- 保存堆内存快照文件：`jmap -dump:live,format=b,file=<filename> <PID>`
- 查看进程：`top`

- 查看线程占用资源情况：`ps H -eo pid,tid,%cpu,%mem |grep <PID>`
- 查看栈日志：`jstack <PID>`

- `ps -fe` 查看所有进程
- `ps -fT -p <PID>` 查看某个进程（`PID`）的所有线程
- `kill` 杀死进程
- `top` 按大写 H 切换是否显示线程
- `top -H -p <PID>` 查看某个进程（`PID`）的所有线程

------

课程：

（1）并发基础：https://www.bilibili.com/video/BV1V4411p7EF?from=search&seid=3162053111718328162

（2）并发高级：https://www.bilibili.com/video/BV1xK4y1C7aT?p=2

