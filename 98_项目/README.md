## `Centos7.5`设置开机自启动

### minio

vi /usr/local/minio/minio.conf

```properties
#volumes to be used for Minio server.
MINIO_VOLUMES="/usr/local/minio/data"
#Use if you want to run Minio on a custom port
MINIO_OPTS="--address :8080"
#Access Key of the server.
MINIO_ROOT_USER=admin
#Access Key of the server.
MINIO_ROOT_PASSWORD=qzq2021_gzxf119
```

vi /etc/systemd/system/minio.service

```properties
[Unit]
Description=MinIO
Documentation=https://docs.min.io
Wants=network-online.target
After=network-online.target
AssertFileIsExecutable=/usr/local/minio/minio

[Service]
WorkingDirectory=/usr/local/minio

User=root
Group=root

EnvironmentFile=-/usr/local/minio/minio.conf
ExecStartPre=/bin/bash -c "if [ -z \"${MINIO_VOLUMES}\" ]; then echo \"Variable MINIO_VOLUMES not set in /etc/default/minio\"; exit 1; fi"
ExecStart=/usr/local/minio/minio server $MINIO_OPTS $MINIO_VOLUMES

# Let systemd restart this service always
Restart=always

# Specifies the maximum file descriptor number that can be opened by this process
LimitNOFILE=65536

# Specifies the maximum number of threads this process can create
TasksMax=infinity

# Disable timeout logic and wait until process is stopped
TimeoutStopSec=infinity
SendSIGKILL=no

[Install]
WantedBy=multi-user.target

# Built for ${project.name}-${project.version} (${project.name})

```

```shell
systemctl enable|disable minio.service
```

### nginx

vi /usr/lib/systemd/system/nginx.service

```properties
[Unit]
Description=nginx
After=network.target

[Service]
Type=forking
ExecStart=/usr/local/nginx/sbin/nginx
ExecReload=/usr/local/nginx/sbin/nginx -s reload
ExecStop=/usr/local/nginx/sbin/nginx -s stop
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

### redis

vim /etc/systemd/system/redis.service

```properties
[Unit]
Description=redis-server
After=network.target
[Service]
Type=forking
ExecStart=/usr/local/redis/bin/redis-server /usr/local/redis/bin/redis.conf
PrivateTmp=true
[Install]
WantedBy=multi-user.target
```

systemctl enable|disable redis.service

### 项目

启动脚本

```shell
#!/bin/bash
JAVA_HOME=/usr/local/jdk-11.0.10
PATH=$PATH:$JAVA_HOME/bin

#替换这里jar包的路径，其它代码无需更改
APP_NAME=/root/qzq/qzq.jar


#使用说明，用来提示输入参数
usage() {
    echo "Usage: sh xxx.sh [start|stop|restart|status]"
    exit 1
}

#检查程序是否在运行
is_exist(){
  pid=`ps -ef|grep $APP_NAME|grep -v grep|awk '{print $2}'`
  #如果不存在返回1，存在返回0
  if [ -z "${pid}" ]; then
   return 1
  else
    return 0
  fi
}

#启动方法，延迟10秒后启动，开机自启动等待其他应用程序先启动
start(){
  is_exist
  if [ $? -eq 0 ]; then
    echo "${APP_NAME} is already running. pid=${pid}"
  else
        echo "${APP_NAME} start in 10 seconds..."
        sleep 10
    nohup java -Djava.awt.headless=true -Xms2048m -Xmx2048m -jar ${APP_NAME} > /root/qzq/logs/nohup-qzq.log 2>&1 &
  fi
}

#停止方法
stop(){
  is_exist
  if [ $? -eq "0" ]; then
    kill -9 $pid
  else
    echo "${APP_NAME} is not running"
  fi
}

#输出运行状态
status(){
  is_exist
  if [ $? -eq "0" ]; then
    echo "${APP_NAME} is running. Pid is ${pid}"
  else
    echo "${APP_NAME} is NOT running."
  fi
}

#重启
restart(){
  stop
  sleep 5
  start
}

#根据输入参数，选择执行对应方法，不输入则执行使用说明
case "$1" in
  "start")
    start
    ;;
  "stop")
    stop
    ;;
  "status")
    status
    ;;
  "restart")
    restart
    ;;
  *)
    usage
    ;;
esac
```

vim /usr/lib/systemd/system/qzq.service

```properties
[Unit]
Description=qzq
After=network.target

[Service]
Type=forking
ExecStart=/root/qzq/restart_qzq.sh start
ExecReload=/root/qzq/restart_qzq.sh restart
ExecStop=/root/qzq/restart_qzq.sh stop
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

systemctl enable qzq.service



查看设置为开机启动的服务：systemctl list-unit-files | grep enabled