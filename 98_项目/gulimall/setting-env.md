### 安装虚拟机

------

- **安装Virtual Box**

  下载地址：https://www.virtualbox.org/

- **安装vagrant**

  下载地址：https://www.vagrantup.com/downloads

- **vagrant安装centos7**

  ```bash
  vagrant init centos7 https://mirrors.ustc.edu.cn/centos-cloud/centos/7/vagrant/x86_64/images/CentOS-7.box
  启动：vagrant up
  ```

- **设置网络**

  ​	找到`C:\Users\yangxu\Vagrantfile`，设置固定ip。

  ​	`config.vm.network "private_network", ip: "192.168.56.10"`

  ​	通过cmd重启vagrant：`vagrant reload`

  ​	cmd通过vagrant进入虚拟机：`vagrant ssh`

  ​	默认账号名和密码：vagrant/vagragnt root/vagrant
  
- 允许外部ssh访问

  vi /etc/ssh/sshd_config

  PasswordAuthentication yes

  service sshd restart 

### 安装Docker

------

-   **官方文档**


  ​	`https://docs.docker.com/engine/install/centos/`

  ```shell
  # 删除旧版本docker
  sudo yum remove docker \
                    docker-client \
                    docker-client-latest \
                    docker-common \
                    docker-latest \
                    docker-latest-logrotate \
                    docker-logrotate \
                    docker-engine
  # 安装依赖包
  sudo yum install -y yum-utils \
  device-mapper-persistent-data\
  lvm2
  # 设置镜像地址
  sudo yum-config-manager \
      --add-repo \
      https://download.docker.com/linux/centos/docker-ce.repo
  ```

-   **配置阿里镜像**


  ​	`https://cr.console.aliyun.com/cn-hangzhou/instances/mirrors`

-   **安装docker**


  ```shell
  # 安装
  sudo yum install -y docker-ce docker-ce-cli containerd.io
  # 启动docker
  sudo systemctl start docker
  # 设置为开机启动
  sudo systemctl enable docker
  ```
### 安装Mysql
------

```shell
# 拉取镜像
docker pull mysql:5.7

# 启动mysql并挂载目录
# -v表示把docker内部的目录挂载到linux的目录文件下
docker run -p 3306:3306 --name mysql \
-v /mydata/mysql/log:/var/log/mysql \
-v /mydata/mysql/data:/var/lib/mysql \
-v /mydata/mysql/conf:/etc/mysql \
-e MYSQL_ROOT_PASSWORD=root \
-d mysql:5.7

# 配置mysql的编码
vi /mydata/mysql/conf/my.cnf

    [client]
    default-character-set=utf8

    [mysql]
    default-character-set=utf8
    [mysqld]
    init_connect='SET colletion_connection = utf8_unicode_ci'
    init_connect='SET NAMES utf8'
    character-set-server=utf8
    collection-server=utf8_unicode_ci
    skip-character-set-client-handshake
    skip-name-resolve
    
# 重启mysql
docker restart mysql
```

### 安装Redis

------

```shell
# 下载redis镜像
docker pull redis

#创建redis配置目录和文件
mkdir -p /mydata/redis/conf
touch /mydata/redis/conf/redis.conf

#运行redis
docker run -p 6379:6379 --name redis -v /mydata/redis/data:/data \
-v/mydata/redis/conf/redis.conf:/etc/redis/redis.conf \
-d redis redis-server /etc/redis/redis.conf

# 测试使用redis镜像执行redis-cli命令连接
docker exec -it redis redis-cli

# 配置redis持久化
vi /mydata/redis/conf/redis.conf
# 配置redis持久化，使用AOF
appendonly yes

#设置开机自动运行
sudo docker update redis --restart=always
```

官方配置文档：`https://redis.io/topics/config`

### 批量创建虚拟机

Vagrantfile

```shell
Vagrant.configure("2") do |config|
   (1..3).each do |i|
        config.vm.define "k8s-node#{i}" do |node|
            # 设置虚拟机的Box
            node.vm.box = "centos/7"

            # 设置虚拟机的主机名
            node.vm.hostname="k8s-node#{i}"

            # 设置虚拟机的IP(public_network：桥接 private_network：仅主机)
            node.vm.network "public_network", ip: "192.168.56.#{99+i}", netmask: "255.255.255.0"

            # 设置主机与虚拟机的共享目录
            # node.vm.synced_folder "~/Documents/vagrant/share", "/home/vagrant/share"

            # VirtaulBox相关配置
            node.vm.provider "virtualbox" do |v|
                # 设置虚拟机的名称
                v.name = "k8s-node#{i}"
                # 设置虚拟机的内存大小
                v.memory = 3072
                # 设置虚拟机的CPU个数
                v.cpus = 2
            end
        end
   end
end
```

### 设置虚拟机网络



### 配置

```sh
#关闭防火墙
systemctl stop firewalld
#关闭selinux
sed -i 's/enforcing/disabled/' /etc/selinux/config
setenforce 0
#关闭swap（内存交换）
sed -ri 's/.*swap.*/#&/' /etc/fstab

#添加主机名与ip的对应关系
vi /etc/hosts
10.0.2.15 k8s-node1
10.0.2.4 k8s-node2
10.0.2.5 k8s-node3
#将桥接的IPv4流量床底到iptables
cat > /etc/sysctl.d/k8s.conf << EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

sysctl --system
```

### 安装docker、kubeadm、kubelet、kubectl

```sh
# 安装docker
# 添加k8s的阿里云YUM源
cat > /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
# 安装k8s
yum install kubelet-1.19.4 kubeadm-1.19.4 kubectl-1.19.4 -y
systemctl enable kubelet
systemctl start kubelet
```

### 部署k8s-master

vi master-images

```shell
#!/bin/bash

images=(
	kube-apiserver:v1.19.4
	kube-proxy:v1.19.4
	kube-controller-manager:v1.19.4
	kube-scheduler:v1.19.4
	coredns:1.6.5
	etcd:3.4.3-0
	pause:3.1
)

for imageName in ${images[@]} ; do
	docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName
done
```



```sh
# master节点初始化，指定service的网络和pod的网络
kubeadm init \
--apiserver-advertise-address=10.0.2.15 \
--image-repository registry.aliyuncs.com/google_containers \
--kubernetes-version v1.19.4 \
--service-cidr=10.96.0.0/12 \
--pod-network-cidr=10.244.0.0/16

mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
#安装pod网络插件。备注：可以通过kubectl delete删除
kubectl apply -f kube-flannel.yml

# 从节点加入主节点，在node机器执行
kubeadm join 10.0.2.15:6443 --token zvvufx.dvfdkqprnn2ovieq \
    --discovery-token-ca-cert-hash sha256:2242bd9fccb962f65d1498f15c06ab86f7c876826b4b09fab373dc5e84727383 
```

### 入门操作

文档地址：https://kubernetes.io/zh/docs/reference/kubectl/overview/

1、部署一个tomcat

kubectl create deployment tomcat6 --image=tomcat:6.0.53-jre8 

Kubectl get pods -o wide 可以获取到 tomcat 信息

2、暴漏端口

kubectl expose deployment tomcat6 --port=80 --target-port=8080 --type=NodePort 

Pod 的 80 映射容器的 8080；service 会代理 Pod 的 80。

kubectl get svc 获取服务以及对外暴漏的端口

3、动态扩容或缩容

kubectl scale --replicas=3 deployment tomcat

4、以上操作的 yaml 获取 

在执行命令后加：--dry-run -o yaml

5、删除 

Kubectl get all 

kubectl delete deploy/tomcat6 

kubectl delete service/tomcat6-service
