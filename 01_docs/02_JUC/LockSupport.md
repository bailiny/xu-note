### 什么是LockSupport

- LockSupport是一个线程阻塞工具类，所有的方法都是静态方法，可以让线程在任意位置阻塞，阻塞之后也有对应的唤醒方法。LockSupport实际调用的Unsafe中的native代码。
- 通过park()和unpark(thread)方法来实现阻塞和唤醒线程的操作。
- 官网解释：
  - LockSupport是用来创建锁和其他同步类的基本线程阻塞原语。
  - LockSupport类使用了一种名为Permit(许可)的概念来做到阻塞和唤醒线程的功能，每个线程都有一个许可(permit)，permit只有两个值1和零,默认是零。
  - 可以把许可看成是一种(0,1)信号量(Semaphore)，但与Semaphore不同的是，许可的累加上限是1。

<img src="../../images/juc/LockSupport1.jpg" style="zoom:80%;" />

```java
private static final sun.misc.Unsafe UNSAFE;

public static void park() {
    UNSAFE.park(false, 0L);
}

public static void unpark(Thread thread) {
    if (thread != null)
        UNSAFE.unpark(thread);
}
```



- 调用unpark(thread)方法后，就会将thread线程的许可permit设置成1(注意多次调用unpark方法，不会累加，permit值还是1)会自动唤醒thread线程，即之前阻塞中的LockSupport.park()方法会立即返回。unpark和park不像sychorized的wait/notify、ReentrantLock的await/signal，不需要严格的先后区分。

### 优点

- LockSupport不用持有锁块,不用加锁,程序性能好
-  没有先后顺序，不容易导致卡死(因为unpark获得了一个凭证，之后再调用park方法，就可以名正言顺的凭证消费，故不会阻塞)
