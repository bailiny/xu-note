### 1. 编写Dockerfile

```dockerfile
FROM openjdk:11-jdk-slim
LABEL maintainer=yangxu

ENV PARAMS="--server.port=8080 --spring.profiles.active=prod"
RUN /bin/cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone

COPY *.jar /app.jar
EXPOSE 8080

ENTRYPOINT ["/bin/sh","-c","java -Dfile.encoding=utf8 -Djava.security.egd=file:/dev/./urandom -jar app.jar ${PARAMS}"]
```

### 2. 制作docker镜像

```shell
# 制作docker镜像
docker build -t poney:v1.0 -f Dockerfile .
# 将本地镜像改成符合规范的名称 注册用户名/镜像名
docker tag poney:v1.0 bailiny/poney:v1.0
# 登录docker hub
docker login
# 上传到docker hub
docker push
```

### 3. 使用kubenetes命令部署

```sh
# 创建一个部署
kubectl create deployment poney --image=bailiny/poney:v1.0 
# 暴漏端口
kubectl expose deployment poney --port=80 --target-port=8080 --type=NodePort
# 查看服务
kubectl get svc -o wide
```

### 4. 使用域名访问

poney-ingress.yaml

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress  
metadata:
  name: ingress-poney
spec:
  ingressClassName: nginx
  rules:
  - host: "poney.eqxiu.com"
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: poney
            port:
              number: 8080
```

查看nginx端口：kubectl get svc -A。访问：http://poney.eqxiu.com:31548/api/doc.html



```sh
docker network inspect bridge
```

