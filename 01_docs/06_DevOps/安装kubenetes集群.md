### 1. 准备环境

- 虚拟机。如果是虚拟机集群则需要设置好网络和以下配置，以下是通过Virtual Box创建的三个虚拟机配置示例：

  网络：网卡1-NAT网络并刷新mac地址。网卡2-仅主机（host-only）网络（如果没有，则在“管理”-“主机网络管理器”中创建一个）。

  ```shell
  #关闭防火墙
  systemctl stop firewalld
  #关闭selinux
  sed -i 's/enforcing/disabled/' /etc/selinux/config
  setenforce 0
  #关闭swap（内存交换）
  sed -ri 's/.*swap.*/#&/' /etc/fstab
  
  #添加主机名与ip的对应关系
  vi /etc/hosts
  10.0.2.15 k8s-node1
  10.0.2.4 k8s-node2
  10.0.2.5 k8s-node3
  #将桥接的IPv4流量到iptables
  cat > /etc/sysctl.d/k8s.conf << EOF
  net.bridge.bridge-nf-call-ip6tables = 1
  net.bridge.bridge-nf-call-iptables = 1
  EOF
  
  sysctl --system
  ```

- 云服务器。如果是云服务器则要创建一个VPC专有网络

### 2. 安装Docker

官方文档：https://docs.docker.com/engine/install/centos

  ```shell
  # 删除旧版本docker
  sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
  # 安装依赖包
  sudo yum install -y yum-utils device-mapper-persistent-data lvm2
  # 设置镜像地址
  sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
  ```

-   **配置阿里镜像**


  	`https://cr.console.aliyun.com/cn-hangzhou/instances/mirrors`

-   **安装docker**


  ```shell
  # 安装
  sudo yum install -y docker-ce docker-ce-cli containerd.io
  # 启动docker
  sudo systemctl start docker
  # 设置为开机启动
  sudo systemctl enable docker
  ```

### 3. 安装kubeadm、kubelet、kubectl

```sh
# 添加k8s的阿里云YUM源
cat > /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
# 安装k8s
yum install kubelet-1.19.4 kubeadm-1.19.4 kubectl-1.19.4 -y
systemctl enable kubelet
systemctl start kubelet
```

### 4. 部署k8s-master

下载镜像：vi master-images。执行脚本下载。

```shell
#!/bin/bash

images=(
	kube-apiserver:v1.19.4
	kube-proxy:v1.19.4
	kube-controller-manager:v1.19.4
	kube-scheduler:v1.19.4
	coredns:1.6.5
	etcd:3.4.3-0
	pause:3.1
)

for imageName in ${images[@]} ; do
	docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/$imageName
done
```

初始化主节点，从节点加入主节点

```sh
# master节点初始化，指定service的网络和pod的网络
kubeadm init \
--apiserver-advertise-address=10.0.2.15 \
--image-repository registry.aliyuncs.com/google_containers \
--kubernetes-version v1.19.4 \
--service-cidr=10.96.0.0/12 \
--pod-network-cidr=10.244.0.0/16

mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
#安装pod网络插件。备注：可以通过kubectl delete删除
kubectl apply -f kube-flannel.yml

# 从节点加入主节点，在node机器执行
kubeadm join 10.0.2.15:6443 --token zvvufx.dvfdkqprnn2ovieq \
    --discovery-token-ca-cert-hash sha256:2242bd9fccb962f65d1498f15c06ab86f7c876826b4b09fab373dc5e84727383 
```

### 5. 入门操作

文档地址：https://kubernetes.io/zh/docs/reference/kubectl/overview/

1、部署一个tomcat

kubectl create deployment tomcat6 --image=tomcat:6.0.53-jre8 

Kubectl get pods -o wide 可以获取到 tomcat 信息

2、暴漏端口

kubectl expose deployment tomcat6 --port=80 --target-port=8080 --type=NodePort 

Pod 的 80 映射容器的 8080；service 会代理 Pod 的 80。

kubectl get svc 获取服务以及对外暴漏的端口

3、动态扩容或缩容

kubectl scale --replicas=3 deployment tomcat

4、以上操作的 yaml 获取 

在执行命令后加：--dry-run -o yaml

5、删除 

Kubectl get all 

kubectl delete deploy/tomcat6 

kubectl delete service/tomcat6-service



```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: tomcat8
  name: tomcat8
spec:
  replicas: 1
  selector:
    matchLabels:
      app: tomcat8
  template:
    metadata:
      labels:
        app: tomcat8
    spec:
      containers:
      - image: tomcat:8.5.73-jre8-temurin-focal
        name: tomcat
```

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-tomcat8
spec:
  ingressClassName: nginx
  rules:
  - host: "tomcat8.eqxiu.com"
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: tomcat8
            port:
              number: 8080
```

